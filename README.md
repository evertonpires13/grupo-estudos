# Grupo de Estudos de Angular


Command line instructions
You can also upload existing files from your computer using the instructions below.

# Git global setup

<ul>
<li>git config --global user.name "nomeDoUsuario"</li>
<li>git config --global user.email "email@email.com"</li>
</ul>
# Create a new repository
<ul>
<li>git clone https://gitlab.com/evertonpires13/grupo-estudos.git</li>
<li>cd grupo-estudos</li>
<li>git switch -c main</li>
<li>touch README.md</li>
<li>git add README.md</li>
<li>git commit -m "add README"</li>
<li>git push -u origin main</li>
</ul>


# Push an existing folder
cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/evertonpires13/grupo-estudos.git
git add .
git commit -m "Initial commit"
git push -u origin main


# Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/evertonpires13/grupo-estudos.git
git push -u origin --all
git push -u origin --tags
